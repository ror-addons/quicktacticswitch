<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="QuickTacticSwitch" version="0.4" date="12/22/2008" >

		<Author name="Encaitar" email="james@orrwhat.net" />
		<Description text="Easy method of switching tactics" />

		<Dependencies>
			<Dependency name="EA_AbilitiesWindow" />
			<Dependency name="EATemplate_DefaultWindowSkin" />
			<Dependency name="LibSlash" />
		</Dependencies>
		
		<Files>
			<File name="quicktacticswitch.lua" />
			<File name="quicktacticswitch.xml" />
		</Files>

		<SavedVariables>
			<SavedVariable name="QTS.KeepTactic" />
		</SavedVariables>
		
		<OnInitialize>
			<CreateWindow name="QuickTacticSwitchWindow" show="false" />
			<CallFunction name="QTS.Initialize" />
		</OnInitialize>
		
		<OnUpdate />

		<OnShutdown/>
	</UiMod>
</ModuleFile>
